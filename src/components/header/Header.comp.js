
import React from 'react';

const Header = ({ currIdx }) => (
    <div style={{backgroundColor: 'yellowgreen'}}>
        <h1>Question no. {currIdx + 1}</h1>
    </div>
)

export default Header;