
import React from 'react';

class Question extends React.Component {
    state={
        checkedIdx: null
    }

    checkAnswer(idx){
        this.setState({
            checkedIdx: idx
        })
    }

    render(){
        return (
            <div style={{backgroundColor: 'pink'}}>
                Q: {this.props.question.question}
                <hr />
                <div style={ansStyle}>
                    {
                        this.props.question.answers.map((ans, i) => (
                            <label key={`answer#${i}`}>
                                <input type="radio" 
                                    checked={i === this.state.checkedIdx} 
                                    onClick={this.checkAnswer.bind(this, i)}
                                />
                                {ans}
                            </label>
                        ))
                    }
                </div>
            </div>
        )
    }
}

const ansStyle = {
    display: 'flex',
    flexDirection: 'column'
}

export default Question;