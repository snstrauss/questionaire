
import React from 'react';

const Navigator = ({ currIdx, lastIdx, nextPressed, prevPressed, donePressed }) => {

    let isLast = currIdx === lastIdx;
    return (
        <div style={navStyle}>
            <button onClick={prevPressed} disabled={currIdx === 0}>prev</button>
            <button onClick={nextPressed}>
                {isLast ? 'done' : 'next'}
            </button>
        </div>
    )
}

const navStyle = {
    backgroundColor: 'dodgerBlue',
    display: 'flex',
    justifyContent: 'space-between'
}

export default Navigator;