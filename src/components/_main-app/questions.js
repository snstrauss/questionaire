
const QuestionsList = [
    {
        "question": "2 * 4 = ?",
        "answers": [12, 8, 3, 22],
        "correct": 8,
        "isCorrect": false
    },
    {
        "question": "5 - 2 = ?",
        "answers": [332, 12, 3, 21],
        "correct": 3,
        "isCorrect": false
    },
    {
        "question": "the word 'House' starts with...?",
        "answers": ["A", "Z", "T", "H"],
        "correct": "H",
        "isCorrect": false
    },
    {
        "question": "The Color of the sky is...?",
        "answers": ["Blue", "Black", "Red", "Changing"],
        "correct": "Changing",
        "isCorrect": false
    }
];

export default QuestionsList;