
import React from 'react';

import Header from '../header/Header.comp.js';
import Question from '../question/Question.comp.js';
import Navigator from '../navigator/Navigator.comp.js';

import QuestionsList from './questions.js';

class App extends React.Component {
    state = {
        questions: [],
        currIdx: 0
    }

    componentWillMount(){
        this.setState({
            questions: QuestionsList
        })
    }

    next(){
        let userAnswerIdx = this.refs[`question#${this.state.currIdx}`].state.checkedIdx;
        let localQuestions = this.state.questions;
        let currQuestion = localQuestions[this.state.currIdx];

        
        if(currQuestion.answers[userAnswerIdx] === currQuestion.correct){
            
           localQuestions[this.state.currIdx].isCorrect = true

        }
        
        let nextIndex = (this.state.currIdx + 1 === this.state.questions.length) ? this.state.currIdx : this.state.currIdx + 1;
        this.setState({
            currIdx: nextIndex,
            questions: localQuestions
        });
        
        if(this.state.currIdx === this.state.questions.length - 1){
            this.done();
        }
    }

    prev(){
        this.setState((prevState) => ({
            currIdx: prevState.currIdx - 1
        }))
    }

    done(){
        let totalCorrect = 0;
        this.state.questions.forEach((question) => {
            if(question.isCorrect){
                totalCorrect++;
            }
        })
        alert(`total correct is ${totalCorrect}`)
    }

    render() {

        return (
            <div>
                <Header currIdx={this.state.currIdx}/>
                {
                    this.state.questions.map((question, i) => (
                        <div style={{display: i === this.state.currIdx ? 'block' : 'none'}}
                             key={`question#${i}`}>
                            <Question ref={`question#${i}`}
                                question={question}
                            />
                            <br />
                        </div>
                    ))
                }
                
                <Navigator 
                    currIdx={this.state.currIdx}
                    lastIdx={this.state.questions.length - 1}
                    nextPressed={this.next.bind(this)}
                    prevPressed={this.prev.bind(this)}
                    donePressed={this.done.bind(this)}
                />
            </div>
        )
    }
}

export default App;