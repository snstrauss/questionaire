import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/_main-app/App.comp.js';

ReactDOM.render(<App />, document.getElementById('root'));